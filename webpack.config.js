
const path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

const _plugins = {
	htmlwebpack: new HtmlWebpackPlugin({
		title: 'SAN Chess',
		minify: {
			collapseWhitespace: true
		}
	}),
	extractText: new ExtractTextPlugin({
		filename: 'bundle.css',
		disable: false,
		allChunks: true
	}),
}

const _rules = {
	sass: {
		test: /\.s?css$/,
		use: ExtractTextPlugin.extract({
			fallback: 'style-loader',
			use: ['css-loader', 'sass-loader']
		})
	},
	babel: {
		test: /\.jsx?$/,
		exclude: /node_modules/,
		use: 'babel-loader',
	}
}


const config = {
	entry: './src/app.jsx',
	output: {
		path: path.resolve(__dirname, "dist"),
		filename: 'bundle.js'
	},
	module: {
		rules: [
			_rules.sass,
			_rules.babel
		]
	},
	plugins: [
		_plugins.extractText
	]
}

module.exports = config;