require('dotenv').config()
let express = require('express')
let app = express()
let path = require('path')
let bodyParser = require('body-parser')
let port = process.env.PORT || 3000
let genuuid = require('uuid/v4');
let routes = require('./private/routes')

// app.use(express.static(path.join(__dirname, 'public')))
app.use(express.static(path.join(__dirname, 'dist')))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

//TODO -- Configure to use DB backed session
let session = require("express-session")({
  genid: (req) => genuuid(),
  secret: process.env.SECRET,
  resave: false,
  saveUninitialized: true,
  cookie: { secure: false }
});

app.use(session);
app.use('/', routes);
//---------------------------------------------------------

let server = app.listen(port, () => console.log('listening on port: ' + port));

let io = require('socket.io').listen(server);

let GameManager = require('./private/GameManager')
let gm = new GameManager(io);
