let cookie = require('cookie');
let Game = require('./Game');
let genuuid = require('uuid/v4');

function getSessionID(socket) {
    let cookies = cookie.parse(socket.request.headers.cookie)
    return cookies['connect.sid'].split(/[\:\.]/g)[1]
}

class GameManager {

    reconnect(sessionID){
        if (sessionID in this.disconnected) {
            //rejoin game
            delete this.disconnected[socket.id];
            //cancel timer on game
        }
    }

    disconnect(socket){
        let gid = this.sockets[socket.id].game;
        if (gid) self.disconnected[sessionID] = gid;                    
        delete this.sockets[socket.id];
        console.log('disconnect fired: ',Object.keys(this.sockets).length, ' players still connected' );
    }

    findMatch(socket){
        if (socket.id in Object.keys(this.lfg)) {
            socket.emit('alreadySearching');
            return;
        };
        //TODO -- add matchmaking criteria        
        let opponent = (Object.keys(lfg) === []) ? false : lfg[0];
        if (!opponent) {
            //add user as searching
            console.log(socket.id, ' now searching for game')
            lfg[socket.id] = socket;
            return;
        } else {
            //opponent found, place both sockets in room and emit game found
            let players = [socket, opponent];
            let game = new Game({
                id: genuuid(),
                white: players.splice(Math.round(Math.random())),
                black: players[0]
            });
            self.games[game.id] = game;

            //allow for reconnection
            self.sockets[socket].game = game.id;
            self.sockets[opponent].game = game.id;

            socket.join(game.id);
            opponent.join(game.id);
        }
    }

    constructor(io) {
        let self = this;
        self.io = io;
        // socket.id -> socket
        self.lfg = {};
        //game id -> game
        self.games = {};
        //socket.id -> session ID , game ID
        self.sockets = {}
        //session ID -> game ID
        self.disconnected = {};
        io.sockets.on('connection', function (socket) {
            let sessionID = getSessionID(socket);
            self.sockets[socket.id] = {sid: sessionID, game: null};
            self.reconnect(sessionID);            
            socket.on('disconnect', () => { self.disconnect(socket) });
            socket.on('findMatch', () => { self.findMatch(socket)}); 
            // socket.on('makeMove', (){})
            // socket.on('resign', self.resign)
        });
    }
}

module.exports = GameManager;