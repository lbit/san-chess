var Chess = require('chess.js').Chess;

class Game{
    constructor(config){
        this.chess = new Chess();
        this.players = {
            white: config.players.white,
            black: config.players.black
        }
    }

    makeMove(m){
        var moves = chess.moves();
        var move = moves[Math.floor(Math.random() * moves.length)];
        chess.move(move);
    }
}

module.exports = Game;
