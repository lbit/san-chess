var express = require('express');
var router = express.Router();
var path = require('path');

router.get('/', function (req, res) {
    res.sendFile(path.join(__dirname + '/../public/index.html'));
})

// var games = {};
// var Chess = require('chess.js').Chess;

// router.post('/move', (req, res) => {
//     let game = getGame(req.sessionID);
//     let result = game.move(req.body.move);
//     if (!result) {
//         res.json({
//             "san": "result.san",
//             "fen": "game.fen()",
//             "valid": !!result,
//             "ascii": game.ascii()
//         });
//     } else {
//         res.json({
//             "san": result.san,
//             "fen": game.fen(),
//             "valid": !!result,
//             "ascii": game.ascii()
//         });

//     }
// })


module.exports = router;
