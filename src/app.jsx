import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import store from './store.js'
import io from 'socket.io-client';


import Home from './pages/Home.jsx'
import InGame from './pages/InGame.jsx'
import PostGame from './pages/PostGame.jsx'

import 'normalize.css'
import 'purecss'
import './app.scss'

ReactDOM.render((
  <Provider store={store}>
    <Router>
      <Switch>
        <Route exact path="/" component={Home}></Route>
        <Route path="/ingame" component={InGame}></Route>
        <Route path="/postgame" component={PostGame}></Route>
      </Switch>
    </Router>
  </Provider>
), document.getElementById('app'));

// const socket = io();

// setInterval(()=>{
//   console.log("fired")
//   socket.emit('fire','')
// },1000);
