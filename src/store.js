import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import promise from 'redux-promise-middleware'
import gameUpdate from './reducers/gameUpdate'
import { createLogger } from 'redux-logger'

const middleware = applyMiddleware(
    promise(),
    createLogger(),
);


const reducers = combineReducers({
    game: gameUpdate
})

let store = createStore(reducers, middleware);
module.exports = store;