const gameUpdate = (state = {lastMove: '--'}, action) => {
    switch (action.type){
        case "MAKE_MOVE_FULFILLED": {
            const result = action.payload.data;
            return result;
        }
    }
    return {...state};
}

export default gameUpdate;