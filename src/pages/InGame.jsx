import React, {Component} from 'react';
import NavBar from '../components/NavBar.jsx';
import Game from '../components/Game.jsx';
import store from '../store.js'
const InGame = () => {
  return (
    <div>
      <NavBar />
      <br/>
      <Game />
    </div>
  )
}

export default InGame;
