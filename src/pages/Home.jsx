import React, {Component} from 'react';
import NavBar from '../components/NavBar.jsx';
import { Link } from 'react-router-dom'

const Home = () => {
  return (
    <div>
      <NavBar />
      <br/>
      <Link to={'/ingame'}><button className='pure-button'>find match</button></Link>
    </div>
  )
}

export default Home;
