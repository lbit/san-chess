import axios from 'axios'

export default (move) => {
    return {
        type: 'MAKE_MOVE',
        payload: axios.post('/move', {"move": move})
    }
}