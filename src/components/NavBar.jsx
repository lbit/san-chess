import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import { withRouter } from 'react-router-dom';

const NavBar = (state) => {
  return (
    <div className = 'navbar'>
      <h1> SAN Chess </h1>
    </div>
  )
}

const mapStateToProps = (state) => {
  return { game: state.game }
}


export default withRouter(connect(mapStateToProps)(NavBar))
