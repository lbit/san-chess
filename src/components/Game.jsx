import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import makeMove from '../actions/makeMove';
import { withRouter } from 'react-router-dom';


export class Game extends Component {

    render() {
        return (
            <div className='game'>
                <p>{this.props.game.valid}</p>
                <input className='pure-group input' type="text" ref={el => this.el = el} /><br/>
                <button className="pure-button" onClick={(e) => this.props.makeMove(this.el.value)}>move</button>
            </div>
        )
    }
}

const mapStateToProps = (state = {}) => {
    return { game: state.game }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ makeMove: makeMove }, dispatch);
}


export default withRouter(connect(mapStateToProps,mapDispatchToProps)(Game))
