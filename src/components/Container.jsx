import React, {Component} from 'react';
import NavBar from './NavBar.jsx';
import Game from './Game.jsx';

const Container = () => {
  return (
    <div>
      <NavBar />
      <br/>
      <Game />
    </div>
  )
}

export default Container;
